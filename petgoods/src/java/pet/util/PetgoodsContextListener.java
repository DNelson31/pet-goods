package pet.util;

import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class PetgoodsContextListener implements ServletContextListener {
	
	
	@Override
   	public void contextInitialized(ServletContextEvent event) {
    	ServletContext sContext = event.getServletContext();
      
      	// initialize the current year attribute
      	GregorianCalendar calendar = new GregorianCalendar();
      	int currentYear = calendar.get(Calendar.YEAR);
      	sContext.setAttribute("currentYear", currentYear);
      

	}
	
	 @Override
	 public void contextDestroyed(ServletContextEvent event) {
		 // not necessary
	 }
	 
}


