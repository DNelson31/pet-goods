package pet.business;

import java.io.Serializable;
import java.text.NumberFormat;

public class Product implements Serializable {
	private Long productid;
	private String code;
	private String name;
	private String description;
	private double price;
	
	public Product() {
		
	}
	
	
	//Getters and Setters
	public Long getProductid() {
		return productid;
	}
	public void setProductid(Long productid) {
		this.productid = productid;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	//Returns price in a currency format
	public String getPriceCurrencyFormat() {
		NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
	    return currencyFormat.format(price);
	}
	
}
