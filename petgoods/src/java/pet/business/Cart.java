package pet.business;

import java.io.Serializable;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class Cart implements Serializable {
	private List<LineItem> items;
	static double salestax = 0.07;
	static double shipping = 5.99;

	   public Cart() {
	      items = new ArrayList<>();
	   }

	   public List<LineItem> getItems() {
	      return items;
	   }

	   public void setItems(List<LineItem> items) {
	      this.items = items;
	   }

	   //return The amount of LineItem objects in the cart
	   public int getSize() {
	      return items.size();
	   }
	   
	   // return The total price for the items in the cart
	   public double getTotalPrice() {
	      double total = 0.0;
	      for (LineItem i : items) {
	         total += i.getTotalPrice();
	      }

	      return total;
	   }
	   
	   //return Formatted String which represents the subtotal price for a cart
	   public String getSubTotalPriceCurrencyFormat() {
	      NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
	      double totalPrice = getTotalPrice();
	      return currencyFormat.format(totalPrice);
	   }
	   
	   //Return Formatted String which represents the tax price for a cart
	   public String getTaxCurrencyFormat() {
		   NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
		   double tax = getTotalPrice() * salestax;
		   return currencyFormat.format(tax);
	   }
	   
	 //Return Formatted String which represents the shipping price for a cart
	   public String getShippingCurrencyFormat() {
		   NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
		   return currencyFormat.format(shipping);
	   }
	   
	   //Return Formatted String which represents the total price for a cart
	   public String getTotalPriceCurrencyFormat() {
		      NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
		      double totalPrice = getTotalPrice() + (getTotalPrice() * salestax) + shipping;
		      return currencyFormat.format(totalPrice);
		   }

	   //Takes the lineitem object and adds a line item into the cart if it's not already there. 
	   public void addItem(LineItem lineItem) {
	      String code = lineItem.getProduct().getCode();
	      int quantity = lineItem.getQuantity();

	      for (LineItem i : items) {
	         if (i.getProduct().getCode().equals(code)) {
	            // already exists
	            i.setQuantity(quantity);
	            return;
	         }
	      }

	      items.add(lineItem);
	   }

	   //Take the lineitem object and removes the item if it exists in the cart, does nothing otherwise
	   public void removeItem(LineItem lineItem) {
	      String code = lineItem.getProduct().getCode();
	      for (int i = 0; i < items.size(); i++) {
	         if (items.get(i).getProduct().getCode().equals(code)) {
	            items.remove(i);
	            return;
	         }
	      }
	   }
}
