package pet.business;

import java.io.Serializable;
import java.text.NumberFormat;
import java.util.Date;
import java.util.List;

public class Invoice implements Serializable {
	private Long number;
    private List<LineItem> lineItems;
    private Customer customer;
    private String invoiceDate;
    private String processed;
    
    public Invoice() {
    	
    }
    
    //Getters and Setters
    public Long getNumber() {
        return number;
     }
    
    public void setNumber(Long number) {
        this.number = number;
     }
    
    public List<LineItem> getLineItems() {
        return lineItems;
     }
    
    public void setLineItems(List<LineItem> lineItems) {
        this.lineItems = lineItems;
     }
    
    public Customer getCustomer() {
        return customer;
     }
    
    public void setCustomer(Customer customer) {
        this.customer = customer;
     }
    
    public String getInvoiceDate() {
        return invoiceDate;
     }
    
    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
     }
    
    public String getProcessed() {
        return processed;
     }
    
    public void setProcessed(String processed) {
        this.processed = processed;
     }
    
    //Returns the total of all products in single invoice
    public double getInvoiceTotal() {
        double total = 0.0;
        for (LineItem i : lineItems) {
           total += i.getTotalPrice();
        }
        
        return total;
     }
    
    //returns the total in a currency format
    public String getInvoiceSubTotalCurrencyFormat() {
        NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
        double total = getInvoiceTotal();
        return currencyFormat.format(total);
     }
    
    //Returns the tax in a currency format
    public String getInvoiceTaxCurrencyFormat() {
        NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
        double total = getInvoiceTotal() * Cart.salestax;
        return currencyFormat.format(total);
     }
    
  //Returns the shipping in a currency format
    public String getInvoiceShippingCurrencyFormat() {
        NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();        
        return currencyFormat.format(Cart.shipping);
     }
    
    //Returns the total including tax and shipping in a currency format
    public String getInvoiceTotalCurrencyFormat() {
        NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
        double total = getInvoiceTotal() + (getInvoiceTotal() * 0.07) + Cart.shipping;
        return currencyFormat.format(total);
     }

}
