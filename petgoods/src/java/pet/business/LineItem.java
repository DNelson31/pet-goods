package pet.business;

import java.io.Serializable;
import java.text.NumberFormat;

public class LineItem implements Serializable {
	   private Long lineItemId;
	   
	   private Product product;
	   
	   private int quantity = 1;
	   
	   public LineItem() {
		   
	   }
	   
	   //Getters and Setters
	   public Long getLineItemId() {
	      return lineItemId;
	   }

	   public Product getProduct() {
	      return product;
	   }

	   public int getQuantity() {
	      return quantity;
	   }
	   
	   public double getTotalPrice() {
	      return product.getPrice() * quantity;
	   } 
	   
	   public String getTotalPriceCurrencyFormat() {
	      NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
	      return currencyFormat.format(getTotalPrice());
	   }
	   
	   public void setLineItemId(Long lineItemId) {
	      this.lineItemId = lineItemId;
	   }

	   public void setProduct(Product product) {
	      this.product = product;
	   }

	   public void setQuantity(int quantity) {
	      this.quantity = quantity;
	   }
}
