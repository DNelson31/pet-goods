package pet.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pet.business.Cart;
import pet.business.LineItem;
import pet.business.Product;
import pet.data.ProductDB;

/**
 * Servlet implementation class UpdateItemServlet
 */
@WebServlet(name = "UpdateItemServlet", urlPatterns ="/updateItem")
public class UpdateItemServlet extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    String url = "/cart.jsp";

	    url = updateItem(request, response); 

	    getServletContext().getRequestDispatcher(url).forward(request, response);
	      
	}
	
	private String updateItem(HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();
	    Cart cart = (Cart) session.getAttribute("cart");
	    String productCode = request.getParameter("productCode");
	    Product product = ProductDB.selectProduct(productCode);

	    int quantity;
	    try {
	    	quantity = Integer.parseInt(request.getParameter("quantity"));
	        if (quantity < 0) {
	        	quantity = 1;
	        }
	    } catch (NumberFormatException e) {
	    	quantity = 1;
	    }

	    if (cart != null && product != null) {
	    	LineItem lineItem = new LineItem();
	        lineItem.setProduct(product);
	        lineItem.setQuantity(quantity);

	        if (quantity > 0) {
	        	cart.addItem(lineItem);
	        } else {
	        	cart.removeItem(lineItem);
	        }
	    }

	    return "/cart.jsp";
	}

}
