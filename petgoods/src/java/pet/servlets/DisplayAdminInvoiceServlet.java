package pet.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pet.business.Invoice;
import pet.data.InvoiceDB;

/**
 * Servlet implementation class DisplayAdminInvoiceServlet
 */
@WebServlet(name = "DisplayAdminInvoiceServlet", urlPatterns = "/displayAdminInvoice")
public class DisplayAdminInvoiceServlet extends HttpServlet {
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        String requestURI = request.getRequestURI();
        String url = "/admin";
        url = displayInvoice(request, response);
      

        getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);
    }
	
	private String displayInvoice(HttpServletRequest request,
            HttpServletResponse response) {

        HttpSession session = request.getSession();

        
        Long invoiceNumber = Long.parseLong(request.getParameter("number"));
        
        Invoice invoice = InvoiceDB.selectInvoice(invoiceNumber);
        session.setAttribute("invoice", invoice);

        return "/admin-invoice.jsp";
    }

}
