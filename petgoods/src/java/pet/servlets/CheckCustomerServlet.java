package pet.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pet.business.Customer;
import pet.data.CustomerDB;
import pet.util.CookieUtil;

/**
 * Servlet implementation class CheckCustomerServlet
 */
@WebServlet(name = "CheckCustomerServlet", urlPatterns = "/checkCustomer")
public class CheckCustomerServlet extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    String url = "/cart.jsp";

	    url = checkCustomer(request, response); 

	    getServletContext().getRequestDispatcher(url).forward(request, response);
	      
	}
	
private String checkCustomer(HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession session = request.getSession();
	    Customer customer = (Customer) session.getAttribute("customer");
	      
	    String url = "/new_customer.jsp";
	    if (customer != null && !customer.getAddress1().equals("")) {
	    // customer with an address exists
	    url = "/displayInvoice";
	    } else {
	    	Cookie[] cookies = request.getCookies();
	    	String email = CookieUtil.getCookieValue(cookies, "emailCookie");

	    	if (email == null) {
	    		customer = new Customer();
	    		url = "/new_customer.jsp";
	    	} else {
	    		customer = CustomerDB.selectByEmail(email);
	    		
	    		if (customer != null && !customer.getAddress1().equals("")) {
	    			url = "/displayInvoice";
	    		} 
	        }
	    }

	    session.setAttribute("customer", customer);
	    return url;
	}

}
