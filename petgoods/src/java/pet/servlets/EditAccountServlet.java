package pet.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pet.business.Customer;
import pet.data.CustomerDB;

/**
 * Servlet implementation class EditAccountServlet
 */
@WebServlet(name = "EditAccountServlet", urlPatterns = "/editAccount")
public class EditAccountServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "/account-index.jsp";
		HttpSession session = request.getSession();
		Customer customer = (Customer) session.getAttribute("customer");
		url = "/accountEdit.jsp";
		
		session.setAttribute("customer", customer);
		getServletContext()
		.getRequestDispatcher(url)
		.forward(request, response);
	
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "/account-index.jsp";
		
		url = updateCustomer(request, response);
		
		getServletContext()
		.getRequestDispatcher(url)
		.forward(request, response);
	
	}

	private String updateCustomer(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		String firstName = request.getParameter("firstName");
	    String lastName = request.getParameter("lastName");
	    String email = request.getParameter("email");
	    String companyName = request.getParameter("companyName");
	    String address1 = request.getParameter("address1");
	    String address2 = request.getParameter("address2");
	    String city = request.getParameter("city");
	    String state = request.getParameter("state");
	    String postCode = request.getParameter("postCode");
	    
	    HttpSession session = request.getSession();
	      Customer customer = createCustomer(session, firstName, lastName, email,
	              companyName, address1, address2, city, state, postCode);
	      
	    // validate data
	    String url;
	    if (isValid(firstName, lastName, email, companyName, address1, address2,
	              city, state, postCode)) {

	    	CustomerDB.update(customer);
	    	url = "/account-index.jsp";
	    } else {
	    	String message = "Unfortunately you either left some fields blank, or "
	                 + "the information provided is not valid.";
	        request.setAttribute("message", message);
	        url = "/accountEdit.jsp";
	    }

	    session.setAttribute("customer", customer);
		return url;
	}

	private boolean isValid(String firstName, String lastName, String email, String companyName, String address1,
			String address2, String city, String state, String postCode) {
		// TODO Auto-generated method stub
		boolean valid = true;

	      if (firstName == null || lastName == null || email == null || address1 == null
	              || city == null || state == null || postCode == null) {
	         valid = false;
	      } else if (firstName.isEmpty() || lastName.isEmpty() || email.isEmpty()
	              || address1.isEmpty() || city.isEmpty() || state.isEmpty()
	              || postCode.isEmpty()) {
	         valid = false;
	      }

	      return valid;
	}

	private Customer createCustomer(HttpSession session, String firstName, String lastName, String email,
			String companyName, String address1, String address2, String city, String state, String postCode) {
		// TODO Auto-generated method stub
		Customer customer = (Customer) session.getAttribute("customer");
	    if (customer == null) {
	    	customer = new Customer();
	    }

	    customer.setFirstName(firstName);
	    customer.setLastName(lastName);
	    customer.setEmail(email);

	    
	    customer.setAddress1(address1);
	    customer.setAddress2(address2);
	    customer.setCompanyName(companyName);
	    customer.setCity(city);
	    customer.setState(state);
	    customer.setPostCode(postCode);

	      
	    return customer;
	}

}
