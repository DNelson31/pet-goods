package pet.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pet.business.Cart;
import pet.business.LineItem;
import pet.business.Product;
import pet.data.ProductDB;

/**
 * Servlet implementation class OrderServlet
 */
@WebServlet(name = "OrderServlet", urlPatterns = {"/Order"})
public class OrderServlet extends HttpServlet {
	@Override
	   protected void doGet(HttpServletRequest request, HttpServletResponse response)
	           throws ServletException, IOException {

	      String requestURI = request.getRequestURI();
	      String url = "/cart.jsp";
	      
	      url = showCart(request, response);

	      getServletContext().getRequestDispatcher(url).forward(request, response);
	   }
	
	
	@Override
	   protected void doPost(HttpServletRequest request, HttpServletResponse response)
	           throws ServletException, IOException {

	      String requestURI = request.getRequestURI();
	      String url = "/cart.jsp";
	      
	      url = showCart(request, response);


	      getServletContext().getRequestDispatcher(url).forward(request, response);
	   }
	
	
	private String showCart(HttpServletRequest request, HttpServletResponse response) {
	    HttpSession session = request.getSession();
	    Cart cart = (Cart) session.getAttribute("cart");

	    if (cart == null || cart.getSize() <= 0) {
	       String emptyMessage = "Your cart is empty";
	       request.setAttribute("emptyMessage", emptyMessage);
	    }

	    return "/cart.jsp";
	 }
	
	
	private String addItem(HttpServletRequest request, HttpServletResponse response) {
	      // retrieve or create a cart
	      HttpSession session = request.getSession();
	      Cart cart = (Cart) session.getAttribute("cart");
	      if (cart == null) {
	         cart = new Cart();
	      }

	      // get the product from the database, create a line item and put it into the cart
	      String productCode = request.getParameter("productCode");
	      Product product = ProductDB.selectProduct(productCode);
	      if (product != null) {
	         LineItem lineItem = new LineItem();
	         lineItem.setProduct(product);
	         cart.addItem(lineItem);
	      }

	      session.setAttribute("cart", cart);
	      return "/cart.jsp";
	   }
	
	

}

