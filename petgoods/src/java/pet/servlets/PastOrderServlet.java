package pet.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pet.business.Customer;
import pet.business.Invoice;
import pet.data.InvoiceDB;

/**
 * Servlet implementation class PastOrderServlet
 */
@WebServlet(name = "PastOrdersServlet", urlPatterns = { "/PastOrders" })
public class PastOrderServlet extends HttpServlet {

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String requestURI = request.getRequestURI();
		HttpSession session = request.getSession();
	    Customer customer = (Customer) session.getAttribute("customer");
	    

        String url = "/account-index.jsp";
        url = displayPastOrders(request, response);

        getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);
	}
	

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String requestURI = request.getRequestURI();
		
		String action = request.getParameter("action");

        String url = "/account-index.jsp";
        url = displayPastOrders(request, response);
        if (action.equals("cancel")) {
        	url = cancelOrder(request, response);
        }

        getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);
	}
	
	private String displayPastOrders(HttpServletRequest request,
            HttpServletResponse response) throws IOException {

		HttpSession session = request.getSession();
	    Customer customer = (Customer) session.getAttribute("customer");
		List<Invoice> customerInvoices
                = InvoiceDB.selectCustomerInvoices(customer);
        
        String url;
        if (customerInvoices != null) {
            if (customerInvoices.size() <= 0) {
                customerInvoices = null;
            }
        }
        
        //HttpSession session = request.getSession();
        session.setAttribute("customerInvoices", customerInvoices);
        url = "/allPastOrders.jsp";
        return url;
    }
	
	private String cancelOrder(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();

        Invoice invoice = (Invoice) session.getAttribute("invoice");
        InvoiceDB.update(invoice, "Cancelled");
        
        Customer customer = (Customer) session.getAttribute("customer");
		List<Invoice> customerInvoices
                = InvoiceDB.selectCustomerInvoices(customer);
 
        if (customerInvoices != null) {
            if (customerInvoices.size() <= 0) {
                customerInvoices = null;
            }
        }
        
        
        session.setAttribute("customerInvoices", customerInvoices);

        return "/allPastOrders.jsp";
	}


}
