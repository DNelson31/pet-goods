package pet.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import pet.business.Customer;
import pet.data.CustomerDB;

/**
 * Servlet implementation class ProcessCustomerServlet
 */
@WebServlet(name = "ProcessCustomerServlet", urlPatterns = "/processCustomer")
public class ProcessCustomerServlet extends HttpServlet {
	@Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) 
    		throws ServletException, IOException {

        String url = "/cart.jsp";
        
        url = processCustomer(request, response);
        
        getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);
    }
	
	private String processCustomer(HttpServletRequest request,
	           HttpServletResponse response) {

	      // retrieve data entered by the customer
	      String firstName = request.getParameter("firstName");
	      String lastName = request.getParameter("lastName");
	      String email = request.getParameter("email");
	      String companyName = request.getParameter("companyName");
	      String address1 = request.getParameter("address1");
	      String address2 = request.getParameter("address2");
	      String city = request.getParameter("city");
	      String state = request.getParameter("state");
	      String postCode = request.getParameter("postCode");

	      HttpSession session = request.getSession();
	      Customer customer = createCustomer(session, firstName, lastName, email,
	              companyName, address1, address2, city, state, postCode);

	      // validate data
	      String url;
	      if (isValid(firstName, lastName, email, companyName, address1, address2,
	              city, state, postCode)) {

	         if (CustomerDB.emailExists(email)) {
	            CustomerDB.update(customer);
	         } else {
	            CustomerDB.insert(customer);
	         }
	         url = "/displayInvoice";
	      } else {
	         String message = "Unfortunately you either left some fields blank, or "
	                 + "the information provided is not valid.";
	         request.setAttribute("message", message);
	         url = "/new_customer.jsp";
	      }

	      session.setAttribute("customer", customer);
	      return url;
	   }
	
	
	private boolean isValid(String firstName, String lastName, String email,
	           String companyName, String address1, String address2, String city,
	           String state, String postCode) {

	      boolean valid = true;

	      if (firstName == null || lastName == null || email == null || address1 == null
	              || city == null || state == null || postCode == null) {
	         valid = false;
	      } else if (firstName.isEmpty() || lastName.isEmpty() || email.isEmpty()
	              || address1.isEmpty() || city.isEmpty() || state.isEmpty()
	              || postCode.isEmpty()) {
	         valid = false;
	      } else {
	    	  
	      }

	      return valid;
	   }
	
	
	private Customer createCustomer(HttpSession session, String firstName,
	           String lastName, String email, String companyName, String address1,
	           String address2, String city, String state, String postCode) {

	      Customer customer = (Customer) session.getAttribute("customer");
	      if (customer == null) {
	         customer = new Customer();
	      }

	      customer.setFirstName(firstName);
	      customer.setLastName(lastName);
	      customer.setEmail(email);

	      customer.setAddress1(address1);
	      customer.setAddress2(address2);
	      customer.setCompanyName(companyName);
	      customer.setCity(city);
	      customer.setState(state);
	      customer.setPostCode(postCode);

	      return customer;
	   }

}
