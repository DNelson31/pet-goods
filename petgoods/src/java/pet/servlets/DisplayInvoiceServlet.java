package pet.servlets;

import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pet.business.Cart;
import pet.business.Customer;
import pet.business.Invoice;

/**
 * Servlet implementation class DisplayInvoiceServlet
 */
@WebServlet(name = "DisplayInvoiceServlet", urlPatterns = "/displayInvoice")
public class DisplayInvoiceServlet extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    String url = "/cart.jsp";

	    url = displayInvoice(request, response); 

	    getServletContext().getRequestDispatcher(url).forward(request, response);
	      
	}
	
	private String displayInvoice(HttpServletRequest request, HttpServletResponse response) {

	      HttpSession session = request.getSession();
	      Customer customer = (Customer) session.getAttribute("customer");
	      Cart cart = (Cart) session.getAttribute("cart");

	      
	      java.time.LocalDateTime myDateObj = java.time.LocalDateTime.now();
	      java.time.format.DateTimeFormatter myFormatObj = java.time.format.DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	      String today = myDateObj.format(myFormatObj);
	      Invoice invoice = new Invoice();
	      invoice.setCustomer(customer);
	      invoice.setLineItems(cart.getItems());
	      invoice.setInvoiceDate(today);

	      session.setAttribute("invoice", invoice);

	      return "/invoice.jsp";
	   }

}
