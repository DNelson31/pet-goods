package pet.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pet.business.Invoice;
import pet.data.InvoiceDB;

/**
 * Servlet implementation class DisplayPastOrderServlet
 */
@WebServlet(name = "DisplayPastOrder", urlPatterns = "/displayPastOrder")
public class DisplayPastOrderServlet extends HttpServlet {
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String url = "/admin";
        url = displayPastOrder(request, response);
      

        getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);
	}

	private String displayPastOrder(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();

		Long invoiceNumber = Long.parseLong(request.getParameter("number"));
        
        Invoice invoice = InvoiceDB.selectInvoice(invoiceNumber);

        session.setAttribute("invoice", invoice);

        return "/pastOrder.jsp";
    }
	

}
