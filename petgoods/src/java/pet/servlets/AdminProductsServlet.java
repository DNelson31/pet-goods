package pet.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pet.business.Product;
import pet.data.ProductDB;

/**
 * Servlet implementation class AdminProductsServlet
 */
@WebServlet(name = "AdminProductsServlet", urlPatterns = "/adminProducts")
public class AdminProductsServlet extends HttpServlet {
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        String requestURI = request.getRequestURI();
        String url = "/admin";
        url = displayProducts(request, response);
        String action = request.getParameter("action");
        
        if (!(action == null)) {
        	if (action.equals("editproduct")) {
        		url = adminEditProduct(request, response);
        	}
        }
      

        getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);
    }


	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        String requestURI = request.getRequestURI();
        String url = "/admin";
        url = displayProducts(request, response);
        String action = request.getParameter("action");
        if (!(action == null)) {
        	if (action.equals("updateproduct")) {
        		url = adminUpdateProduct(request, response);
        	} else if (action.equals("deleteproduct")) {
        		url = adminDeleteProduct(request, response);
        	} else if (action.equals("addproduct")) {
        		url = adminAddProduct(request, response);
        	}
        }

        getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);
    }
	


	private String displayProducts(HttpServletRequest request, HttpServletResponse response) {

	      List<Product> oneList = ProductDB.selectProducts();
	      HttpSession session = request.getSession();
	      session.setAttribute("allProductList", oneList);
	      

	      return "/admin-products.jsp";
	 }
	
	private String adminEditProduct(HttpServletRequest request, HttpServletResponse response) {
		String productCode = request.getParameter("productCode");
		Product oneList = ProductDB.selectProduct(productCode);
		request.setAttribute("allProductList", oneList);
		return "/adminEditProduct.jsp";
	}
	
	private String adminUpdateProduct(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		
		String productName = request.getParameter("productName");
		String productDescription = request.getParameter("productDescription");
		double productPrice = Double.parseDouble(request.getParameter("productPrice"));
		String productCode = request.getParameter("productCode");		

		HttpSession session = request.getSession();
	      
		Product product = createProduct(session,productName, productDescription, productPrice, productCode);

	      
		ProductDB.update(product);
		List<Product> oneList = ProductDB.selectProducts();
	    session.setAttribute("allProductList", oneList);
	    

	    return "/admin-products.jsp";
	}
	
	private Product createProduct(HttpSession session, String productName,
	           String productDescription, double productPrice, String productCode) {

	      
	      Product product = (Product) session.getAttribute("product");
	    
	      
	      if (product == null) {
	      	product = new Product();
	      }

	      product.setName(productName);
	      product.setDescription(productDescription);
	      product.setPrice(productPrice);
	      product.setCode(productCode);
	      
	      
	      return product;
	      
	      
	 }
	
	private String adminDeleteProduct(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		String productCode = request.getParameter("productCode");
		ProductDB.delete(productCode);
		List<Product> oneList = ProductDB.selectProducts();
	    HttpSession session = request.getSession();
	    session.setAttribute("allProductList", oneList);
		return "/admin-products.jsp";
		
	}
	
	private String adminAddProduct(HttpServletRequest request, HttpServletResponse response) {
		String productName = request.getParameter("productName");
		String productDescription = request.getParameter("productDescription");
		double productPrice = Double.parseDouble(request.getParameter("productPrice"));
		String productCode = request.getParameter("productCode");
		
		Product product = new Product();
		product.setCode(productCode);
		product.setName(productName);
		product.setDescription(productDescription);
		product.setPrice(productPrice);
		
		ProductDB.insert(product);
		
		
		List<Product> oneList = ProductDB.selectProducts();
		HttpSession session = request.getSession();
	    session.setAttribute("allProductList", oneList);
	    

	    return "/admin-products.jsp";
	}


}
