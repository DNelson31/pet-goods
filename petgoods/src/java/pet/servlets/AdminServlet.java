package pet.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pet.business.Invoice;
import pet.data.InvoiceDB;

/**
 * Servlet implementation class AdminServlet
 */
@WebServlet(name = "AdminServlet", urlPatterns = "/admin")
public class AdminServlet extends HttpServlet {
	@Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        String requestURI = request.getRequestURI();
        String url = "/admin";
        String action = request.getParameter("action");
        
        url = displayInvoices(request, response);
        if (!(action == null)) {
        	if (action.equals("processed")) {
        		url = displayProcessedInvoices(request, response);
        	} else if (action.equals("unprocessed")) {
        		url = displayUnprocessedInvoices(request, response);
        	} else if (action.equals("cancelled")) {
        		url = displayCancelledInvoices(request, response);
        	}
        }
        

        getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);
    }
	


	@Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        String requestURI = request.getRequestURI();
        String url = "/admin";
        url = displayInvoices(request, response);
      

        getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);
    }
	
	private String displayUnprocessedInvoices(HttpServletRequest request,
            HttpServletResponse response) throws IOException {

        List<Invoice> unprocessedInvoices
                = InvoiceDB.selectUnprocessedInvoices();
        
        String url;
        if (unprocessedInvoices != null) {
            if (unprocessedInvoices.size() <= 0) {
                unprocessedInvoices = null;
            }
        }
        
        HttpSession session = request.getSession();
        session.setAttribute("Invoices", unprocessedInvoices);
        url = "/admin-invoices.jsp";
        return url;
    }
	
	private String displayInvoices(HttpServletRequest request,
            HttpServletResponse response) throws IOException {

        List<Invoice> allInvoices
                = InvoiceDB.selectAllInvoices();
        
        String url;
        if (allInvoices != null) {
            if (allInvoices.size() <= 0) {
                allInvoices = null;
            }
        }
        
        HttpSession session = request.getSession();
        session.setAttribute("Invoices", allInvoices);
        url = "/admin-invoices.jsp";
        return url;
    }
	
	private String displayProcessedInvoices(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		List<Invoice> processedInvoices
        		= InvoiceDB.selectProcessedInvoices();

		String url;
		if (processedInvoices != null) {
			if (processedInvoices.size() <= 0) {
				processedInvoices = null;
			}
		}

		HttpSession session = request.getSession();
		session.setAttribute("Invoices", processedInvoices);
		url = "/admin-invoices.jsp";
		return url;
	}
	
	private String displayCancelledInvoices(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		List<Invoice> cancelledInvoices
				= InvoiceDB.selectCancelledInvoices();

		String url;
		if (cancelledInvoices != null) {
			if (cancelledInvoices.size() <= 0) {
				cancelledInvoices = null;
			}
		}

		HttpSession session = request.getSession();
		session.setAttribute("Invoices", cancelledInvoices);
		url = "/admin-invoices.jsp";
		return url;
	}
	
	

}
