package pet.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import pet.business.Product;
import pet.data.ProductDB;

/**
 * Servlet implementation class SingleProductServlet
 */
@WebServlet(name = "SingleProductServlet", urlPatterns = {"/SingleProduct"})
public class SingleProductServlet extends HttpServlet {
	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
	String productCode = request.getParameter("productCode");
	Product oneList = ProductDB.selectProduct(productCode);
	request.setAttribute("allProductList", oneList);

	RequestDispatcher dispatcher = request.getRequestDispatcher("product-details.jsp");
	dispatcher.forward(request, response);

    }
}
