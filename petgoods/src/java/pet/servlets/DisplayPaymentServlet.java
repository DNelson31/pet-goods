package pet.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class DisplayPaymentServlet
 */
@WebServlet(name = "DisplayPaymentServlet", urlPatterns = "/displayPayment")
public class DisplayPaymentServlet extends HttpServlet {
	@Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) 
    		throws ServletException, IOException {

        String url = "/payment.jsp";
        
        getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);
    }

}
