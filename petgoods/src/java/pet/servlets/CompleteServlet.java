package pet.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import pet.business.Customer;
import pet.business.Invoice;
import pet.data.CustomerDB;
import pet.data.InvoiceDB;
import pet.util.CookieUtil;

/**
 * Servlet implementation class CompleteServlet
 */
@WebServlet(name = "CompleteServlet", urlPatterns = "/complete")
public class CompleteServlet extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	    String url = "/cart.jsp";
	     
	    url = complete(request, response);

	    getServletContext().getRequestDispatcher(url).forward(request, response);
	}
	
	private String complete(HttpServletRequest request, HttpServletResponse response) {
	      // retrieve parametres from the payment form
	      String cardType = request.getParameter("cardType");
	      String cardNumber = request.getParameter("cardNumber");
	      String expirationMonth = request.getParameter("expirationMonth");
	      String expirationYear = request.getParameter("expirationYear");

	      HttpSession session = request.getSession();
	      Customer customer = (Customer) session.getAttribute("customer");
	      Invoice invoice = (Invoice) session.getAttribute("invoice");

	      //CreditCard creditCard = new CreditCard();
	      customer.setCardType(cardType);
	      customer.setNumber(cardNumber);
	      customer.setExpirationDate(expirationMonth + "/" + expirationYear);

	      
	      if (CustomerDB.emailExists(customer.getEmail())) {
	         CustomerDB.update(customer);
	      } else {
	         CustomerDB.insert(customer);
	      }
	      request.setAttribute("name", customer.getFirstName());

	      // insert an invoice to the database and remove all items from user's cart
	      InvoiceDB.insert(invoice);
	      session.setAttribute("cart", null);
	      Cookie[] cookies = request.getCookies();
	      String email = CookieUtil.getCookieValue(cookies, "emailCookie");
		  if (email == null) {
			  session.setAttribute("customer", null);
		  }
	     
	      
	      return "/thanks.jsp";
	   }


}
