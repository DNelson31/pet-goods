package pet.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pet.business.Customer;
import pet.data.CustomerDB;
import pet.data.AccountDB;

/**
 * Servlet implementation class RegisterAccountServlet
 */
@WebServlet(name = "RegisterAccountServlet", urlPatterns = "/registerAccount")
public class RegisterAccountServlet extends HttpServlet {

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String url = "/register.jsp";
		
		url = registerAccount(request, response);
		
		getServletContext()
        .getRequestDispatcher(url)
        .forward(request, response);
	}

	private String registerAccount(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		String firstname = request.getParameter("firstname");
		String lastname = request.getParameter("lastname");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		Customer customer = createCustomer(firstname, lastname, email);
		
		String url = null;
		String message = null;
		if (AccountDB.emailExists(email, "customer")) {
			message = "Email already exists";
			url = "/register.jsp";
		} else if (!password.equals(password2)) {
			message = "Passwords do not match";
			url = "/register.jsp";
		} else {
			if (!CustomerDB.emailExists(email)) {
				CustomerDB.insert(customer);
			}
			AccountDB.insert(email, password, "customer");
			url = "/login.jsp";
			message = "Account created, please login";
		}
		request.setAttribute("message", message);
		
		return url;
		
	}
	
	private Customer createCustomer(String firstname, String lastname, String email) {

	      Customer customer = new Customer();

	      customer.setFirstName(firstname);
	      customer.setLastName(lastname);
	      customer.setEmail(email);

	      return customer;
	   }

}
