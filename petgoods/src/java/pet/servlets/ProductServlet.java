package pet.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pet.business.Customer;
import pet.business.Product;
import pet.data.CustomerDB;
import pet.data.ProductDB;

/**
 * Servlet implementation class ProductServlet
 */


@WebServlet(name = "ProductServlet", urlPatterns = {"/Products", })
public class ProductServlet extends HttpServlet {
	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String requestURI = request.getRequestURI();
	      String url = "/index.html";
	      url = displayProducts(request, response);
	      String action = request.getParameter("action");
	      
	      if (action != null) {
	    	  if (action.equals("lowtohigh")) {
	    		  url = displayProductsLowToHigh(request, response);
	    	  } else if (action.equals("hightolow")) {
	    		  url = displayProductsHighToLow(request, response);
	    	  }
	      }
	      

	      getServletContext().getRequestDispatcher(url).forward(request, response);
	}
	
	 @Override
	   protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	      String requestURI = request.getRequestURI();
	      String url = "/Products";

	      url = registerCustomer(request, response);

	      getServletContext().getRequestDispatcher(url).forward(request, response);
	   }
	 
	 
	 private String displayProducts(HttpServletRequest request, HttpServletResponse response) {

	      List<Product> oneList = ProductDB.selectProducts();
	      HttpSession session = request.getSession();
	      session.setAttribute("allProductList", oneList);
	    

	      return "/products.jsp";
	 }
	 
	 private String displayProductsLowToHigh(HttpServletRequest request, HttpServletResponse response) {

	      List<Product> oneList = ProductDB.selectProductsLow();
	      HttpSession session = request.getSession();
	      session.setAttribute("allProductList", oneList);
	    

	      return "/products.jsp";
	 }
	 
	 private String displayProductsHighToLow(HttpServletRequest request, HttpServletResponse response) {

	      List<Product> oneList = ProductDB.selectProductsHigh();
	      HttpSession session = request.getSession();
	      session.setAttribute("allProductList", oneList);
	    

	      return "/products.jsp";
	 }
	 
	 
	 private String registerCustomer(HttpServletRequest request, HttpServletResponse response) {

	      String firstName = request.getParameter("firstName");
	      String lastName = request.getParameter("lastName");
	      String email = request.getParameter("email");

	      // Validate input
	      String message;
	      if (!isValid(firstName, lastName, email)) {
	         message = "Unfortunately some details you have entered are not valid.";
	         request.setAttribute("message", message);
	         return "/register.jsp";
	      }

	      Customer customer = new Customer();
	      customer.setFirstName(firstName);
	      customer.setLastName(lastName);
	      customer.setEmail(email);

	      if (CustomerDB.emailExists(email)) {
	         CustomerDB.update(customer);
	      } else {
	         CustomerDB.insert(customer);
	      }

	      HttpSession session = request.getSession();
	      session.setAttribute("customer", customer);

	      Cookie emailCookie = new Cookie("emailCookie", email);
	      emailCookie.setMaxAge(60 * 60 * 24 * 365 * 2);
	      emailCookie.setPath("/");
	      response.addCookie(emailCookie);
	      
	      List<Product> oneList = ProductDB.selectProducts();
	      
	      request.setAttribute("allProductList", oneList);

	      
	      return "/products.jsp";
	   }
	 
	 private boolean isValid(String firstName, String lastName, String email) {
	      boolean valid = true;

	      if (firstName == null || firstName.isEmpty()) {
	         valid = false;
	      } else if (lastName == null || lastName.isEmpty()) {
	         valid = false;
	      } else {
	        
	      }

	      return valid;
	   }

}
