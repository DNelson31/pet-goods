package pet.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pet.business.Customer;
import pet.data.CustomerDB;
import pet.util.CookieUtil;
import pet.data.AccountDB;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(name = "LoginServlet", urlPatterns = "/login")
public class LoginServlet extends HttpServlet {

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "/register.jsp";
		HttpSession session = request.getSession();
	    Customer customer = (Customer) session.getAttribute("customer");
	    String action = request.getParameter("action");
	    
	    if (action.equals("login")) {
	    	url = login(request, response);
	    } else if (action.equals("adminlogin")) {
	    	url = adminLogin(request, response);
	    }
	    
		getServletContext()
        .getRequestDispatcher(url)
        .forward(request, response);
		
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String url = "/register.jsp";
		/*HttpSession session = request.getSession();
	    Customer customer = (Customer) session.getAttribute("customer");*/
	    Cookie[] cookies = request.getCookies();
    	String email = CookieUtil.getCookieValue(cookies, "emailCookie");
	    if (email == null) {
	    	url = "/login.jsp";
	    } else {
	    	url = "/account-index.jsp";
	    }
		
		getServletContext()
        .getRequestDispatcher(url)
        .forward(request, response);
		
	}

	private String login(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		
		String url = null;
		String message = null;
		
		if (AccountDB.login(email, password, "customer")) {
			Customer customer = CustomerDB.selectByEmail(email);
			
			HttpSession session = request.getSession();
		    session.setAttribute("customer", customer);
			Cookie emailCookie = new Cookie("emailCookie", email);
		    emailCookie.setMaxAge(60 * 24 * 365 * 2 * 60);
		    emailCookie.setPath("/");
		    response.addCookie(emailCookie);
			url = "/account-index.jsp";
		} else {
			message = "Email and/or passowrd invalid";
			url = "/login.jsp";
		}
		request.setAttribute("message", message);
		return url;
	}
	
	private String adminLogin(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		
		String url = null;
		String message = null;
		
		if (AccountDB.login(email, password, "admin")) {
			
			Cookie adminEmailCookie = new Cookie("adminEmailCookie", email);
		    adminEmailCookie.setMaxAge(60 * 24 * 365 * 2 * 60);
		    adminEmailCookie.setPath("/");
		    response.addCookie(adminEmailCookie);
			url = "/admin-index.jsp";
		} else {
			message = "Email and/or passowrd invalid";
			url = "/admin-login.jsp";
		}
		request.setAttribute("message", message);
		return url;
	}
	

}
