package pet.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pet.business.Cart;
import pet.business.LineItem;
import pet.business.Product;
import pet.data.ProductDB;

/**
 * Servlet implementation class AddItemServlet
 */
@WebServlet(name = "AddItemServlet", urlPatterns = "/addItem")
public class AddItemServlet extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    String url = "/cart.jsp";

	    url = addItem(request, response); 

	    getServletContext().getRequestDispatcher(url).forward(request, response);
	      
	}
	
	
	
	private String addItem(HttpServletRequest request, HttpServletResponse response) {
	      // retrieve or create a cart
	      HttpSession session = request.getSession();
	      Cart cart = (Cart) session.getAttribute("cart");
	      if (cart == null) {
	         cart = new Cart();
	      }

	      // get the product from the database, create a line item and put it into the cart
	      String productCode = request.getParameter("productCode");
	      Product product = ProductDB.selectProduct(productCode);
	      int quantity = Integer.parseInt(request.getParameter("quantity"));
	      if (product != null) {
	         LineItem lineItem = new LineItem();
	         lineItem.setProduct(product);
	         lineItem.setQuantity(quantity);
	         cart.addItem(lineItem);
	      }
	      
	      session.setAttribute("cart", cart);
	      return "/cart.jsp";
	   }
}
