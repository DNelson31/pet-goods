package pet.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pet.business.Invoice;
import pet.data.InvoiceDB;

/**
 * Servlet implementation class ProcessInvoiceServlet
 */
@WebServlet(name = "ProcessInvoiceServlet", urlPatterns = "/processInvoice")
public class ProcessInvoiceServlet extends HttpServlet {
	@Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        String requestURI = request.getRequestURI();
        String url = "/admin";
        url = processInvoice(request, response);

        getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);
    }
	
	private String processInvoice(HttpServletRequest request,
            HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();

        Invoice invoice = (Invoice) session.getAttribute("invoice");
        InvoiceDB.update(invoice, "Shipped");

        return "/admin";
    }

}
