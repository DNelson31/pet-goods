package pet.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import pet.business.Cart;
import pet.business.LineItem;
import pet.business.Product;
import pet.data.ProductDB;

/**
 * Servlet implementation class RemoveItemServlet
 */
@WebServlet(name = "RemoveItemServlet", urlPatterns = "/removeItem")
public class RemoveItemServlet extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    String url = "/cart.jsp";

	    url = removeItem(request, response); 

	    getServletContext().getRequestDispatcher(url).forward(request, response);
	      
	}
	
	private String removeItem(HttpServletRequest request, HttpServletResponse response) {

		HttpSession session = request.getSession();
	    Cart cart = (Cart) session.getAttribute("cart");
	    String productCode = request.getParameter("productCode");
	    Product product = ProductDB.selectProduct(productCode);

	    if (cart != null && product != null) {
	    	LineItem lineItem = new LineItem();
	        lineItem.setProduct(product);
	        cart.removeItem(lineItem);
	    }

	    return "/Order";
	}

}
