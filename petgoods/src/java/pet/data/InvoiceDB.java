package pet.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import pet.business.Customer;
import pet.business.Invoice;
import pet.business.LineItem;

public class InvoiceDB {
	/*
	 * Inserts invoice instance into database
	 */
	public static void insert(Invoice invoice) {
		String query = "INSERT INTO Invoice (CUSTOMER_CUSTOMERID, InvoiceDate) VALUES ("
				+ invoice.getCustomer().getCustomerId() + ", '"
				+ invoice.getInvoiceDate() + "')";
		

      	try {
      		DBUtil.execute(query);
      		
      		Connection con = DBUtil.getConnection();
      		//DBUtil.execute(query);
      		String identityQuery = "SELECT number from invoice where invoicedate = '" + invoice.getInvoiceDate() + "'";
            Statement identityStatement = con.createStatement();
            ResultSet identityResultSet = identityStatement.executeQuery(identityQuery);
            identityResultSet.next();
            Long invoiceID = identityResultSet.getLong("number");
            invoice.setNumber(invoiceID);
            identityResultSet.close();
            identityStatement.close();
            con.close();
      		
      		List<LineItem> lineItems = invoice.getLineItems();
            for (LineItem item : lineItems) {
                LineItemDB.insert(invoiceID, item);
            }
      		
      	} catch (Exception e) {
      		// TODO: handle exception
      		e.printStackTrace();
      	}
	}
	
	/*
	 * returns invoice instance based on invoice number
	 */
	public static Invoice selectInvoice(Long invoiceNumber) {
		String query = "SELECT * "
				+ "FROM Customer "
				+ "INNER JOIN Invoice "
				+ "ON Customer.customerid = Invoice.customer_customerid "
				+ "WHERE invoice.number = " + invoiceNumber;
		try {
        	Connection con = DBUtil.getConnection();
    	    Statement stmt = con.createStatement();
    	    ResultSet rs = stmt.executeQuery(query);
    	    
            Invoice invoice = null;
            
            while (rs.next()) {
                //Create a User object
                Customer customer = CustomerDB.selectByEmail(rs.getString("Email"));

                //Get line items
                long invoiceID = rs.getLong("number");
                List<LineItem> lineItems = LineItemDB.selectLineItems(invoiceID);

                //Create the Invoice object
                invoice = new Invoice();
                invoice.setCustomer(customer);
                invoice.setInvoiceDate(rs.getString("InvoiceDate"));
                invoice.setNumber(invoiceID);
                invoice.setLineItems(lineItems);
                invoice.setProcessed(rs.getString("processed"));

            }
            stmt.close();
			con.close();
            return invoice;
        } catch (SQLException e) {
            System.err.println(e);
            return null;
        }
		
	}
	
	public static ArrayList<Invoice> selectAllInvoices() {
        

        //This method reads in all invoices 
        //To do this, it creates a ArrayList<Invoice> of
        //Invoice objects, which each contain a User object.
        //This method returns null if no invoices are found.
		String query = "SELECT * "
				+ "FROM Customer "
				+ "INNER JOIN Invoice "
				+ "ON Customer.customerid = Invoice.customer_customerid "
				+ "ORDER BY InvoiceDate";
        try {
        	Connection con = DBUtil.getConnection();
    	    Statement stmt = con.createStatement();
    	    ResultSet rs = stmt.executeQuery(query);
    	    
            ArrayList<Invoice> allInvoices = new ArrayList<>();
            
            while (rs.next()) {
                //Create a User object
                Customer customer = CustomerDB.selectByEmail(rs.getString("Email"));

                //Get line items
                long invoiceID = rs.getLong("number");
                List<LineItem> lineItems = LineItemDB.selectLineItems(invoiceID);

                //Create the Invoice object
                Invoice invoice = new Invoice();
                invoice.setCustomer(customer);
                invoice.setInvoiceDate(rs.getString("InvoiceDate"));
                invoice.setNumber(invoiceID);
                invoice.setLineItems(lineItems);
                invoice.setProcessed(rs.getString("processed"));

                allInvoices.add(invoice);
            }
            stmt.close();
			con.close();
            return allInvoices;
        } catch (SQLException e) {
            System.err.println(e);
            return null;
        } 
    }
	
	public static ArrayList<Invoice> selectUnprocessedInvoices() {
        

        //This method reads in all invoices that have not been
        //processed yet. To do this, it creates a ArrayList<Invoice> of
        //Invoice objects, which each contain a User object.
        //This method returns null if no unprocessed invoices are found.
		String query = "SELECT * "
				+ "FROM Customer "
				+ "INNER JOIN Invoice "
				+ "ON Customer.customerid = Invoice.customer_customerid "
				+ "WHERE Invoice.processed = 'Not Shipped'";
        try {
        	Connection con = DBUtil.getConnection();
    	    Statement stmt = con.createStatement();
    	    ResultSet rs = stmt.executeQuery(query);
    	    
            ArrayList<Invoice> unprocessedInvoices = new ArrayList<>();
            
            while (rs.next()) {
                //Create a User object
                Customer customer = CustomerDB.selectByEmail(rs.getString("Email"));

                //Get line items
                long invoiceID = rs.getLong("number");
                List<LineItem> lineItems = LineItemDB.selectLineItems(invoiceID);

                //Create the Invoice object
                Invoice invoice = new Invoice();
                invoice.setCustomer(customer);
                invoice.setInvoiceDate(rs.getString("InvoiceDate"));
                invoice.setNumber(invoiceID);
                invoice.setLineItems(lineItems);
                invoice.setProcessed(rs.getString("processed"));

                unprocessedInvoices.add(invoice);
            }
            stmt.close();
			con.close();
            return unprocessedInvoices;
        } catch (SQLException e) {
            System.err.println(e);
            return null;
        } 
    }
	
	public static ArrayList<Invoice> selectProcessedInvoices() {
        

        //This method reads in all invoices that have been processed.
		//To do this, it creates a ArrayList<Invoice> of
        //Invoice objects, which each contain a User object.
        //This method returns null if no processed invoices are found.
		String query = "SELECT * "
				+ "FROM Customer "
				+ "INNER JOIN Invoice "
				+ "ON Customer.customerid = Invoice.customer_customerid "
				+ "WHERE Invoice.processed = 'Shipped'";
        try {
        	Connection con = DBUtil.getConnection();
    	    Statement stmt = con.createStatement();
    	    ResultSet rs = stmt.executeQuery(query);
    	    
            ArrayList<Invoice> unprocessedInvoices = new ArrayList<>();
            
            while (rs.next()) {
                //Create a User object
                Customer customer = CustomerDB.selectByEmail(rs.getString("Email"));

                //Get line items
                long invoiceID = rs.getLong("number");
                List<LineItem> lineItems = LineItemDB.selectLineItems(invoiceID);

                //Create the Invoice object
                Invoice invoice = new Invoice();
                invoice.setCustomer(customer);
                invoice.setInvoiceDate(rs.getString("InvoiceDate"));
                invoice.setNumber(invoiceID);
                invoice.setLineItems(lineItems);
                invoice.setProcessed(rs.getString("processed"));

                unprocessedInvoices.add(invoice);
            }
            stmt.close();
			con.close();
            return unprocessedInvoices;
        } catch (SQLException e) {
            System.err.println(e);
            return null;
        } 
    }
	
	public static ArrayList<Invoice> selectCancelledInvoices() {
        

        //This method reads in all invoices that have been processed.
		//To do this, it creates a ArrayList<Invoice> of
        //Invoice objects, which each contain a User object.
        //This method returns null if no processed invoices are found.
		String query = "SELECT * "
				+ "FROM Customer "
				+ "INNER JOIN Invoice "
				+ "ON Customer.customerid = Invoice.customer_customerid "
				+ "WHERE Invoice.processed = 'Cancelled'";
        try {
        	Connection con = DBUtil.getConnection();
    	    Statement stmt = con.createStatement();
    	    ResultSet rs = stmt.executeQuery(query);
    	    
            ArrayList<Invoice> cancelledInvoices = new ArrayList<>();
            
            while (rs.next()) {
                //Create a User object
                Customer customer = CustomerDB.selectByEmail(rs.getString("Email"));

                //Get line items
                long invoiceID = rs.getLong("number");
                List<LineItem> lineItems = LineItemDB.selectLineItems(invoiceID);

                //Create the Invoice object
                Invoice invoice = new Invoice();
                invoice.setCustomer(customer);
                invoice.setInvoiceDate(rs.getString("InvoiceDate"));
                invoice.setNumber(invoiceID);
                invoice.setLineItems(lineItems);
                invoice.setProcessed(rs.getString("processed"));

                cancelledInvoices.add(invoice);
            }
            stmt.close();
			con.close();
            return cancelledInvoices;
        } catch (SQLException e) {
            System.err.println(e);
            return null;
        } 
    }
	
	public static ArrayList<Invoice> selectCustomerInvoices(Customer customer) {
        

        /*
         * selects an invoice based on the customer 
         */
		String query = "SELECT * "
				+ "FROM Customer "
				+ "INNER JOIN Invoice "
				+ "ON Customer.customerid = Invoice.customer_customerid "
				+ "WHERE Customer.customerid = " + customer.getCustomerId()
				+ " ORDER BY InvoiceDate";
        try {
        	Connection con = DBUtil.getConnection();
    	    Statement stmt = con.createStatement();
    	    ResultSet rs = stmt.executeQuery(query);
    	    
            ArrayList<Invoice> customerInvoices = new ArrayList<>();
            
            while (rs.next()) {
                //Create a User object
                customer = CustomerDB.selectByEmail(rs.getString("Email"));

                //Get line items
                long invoiceID = rs.getLong("number");
                List<LineItem> lineItems = LineItemDB.selectLineItems(invoiceID);

                //Create the Invoice object
                Invoice invoice = new Invoice();
                invoice.setCustomer(customer);
                invoice.setInvoiceDate(rs.getString("InvoiceDate"));
                invoice.setNumber(invoiceID);
                invoice.setLineItems(lineItems);
                invoice.setProcessed(rs.getString("processed"));

                customerInvoices.add(invoice);
            }
            stmt.close();
			con.close();
            return customerInvoices;
        } catch (SQLException e) {
            System.err.println(e);
            return null;
        } 
    }
	
	/*
	 * updates invoice instance with the specified order status
	 */
	public static void update(Invoice invoice, String Status) {
        String query = "UPDATE Invoice SET "
                + "Processed = '"
                + Status + "' "
                + "WHERE number = " + invoice.getNumber();
        try {
           DBUtil.execute(query); ;
        } catch (SQLException e) {
            System.err.println(e);
        }
    }
}
