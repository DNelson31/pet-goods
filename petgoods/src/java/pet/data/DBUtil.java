package pet.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DBUtil {
	
    static Connection getConnection()
    {
	String jdbcURL = "jdbc:derby:C:\\Users\\nelso\\eclipse-workspace\\petgoods\\webapp\\petgoodsDB;create=true";

	try
	{
	    Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
	    Connection con = DriverManager.getConnection(jdbcURL);
	    return con;
	} catch (SQLException | ClassNotFoundException e)
	{
	    // TODO Auto-generated catch block
	    e.printStackTrace();

	}

	return null;
    }

    static void execute(String sql) throws SQLException
    {
	// Creating the Statement object
	Connection con = getConnection();
	Statement stmt = null;
	try
	{
	    stmt = con.createStatement();
	    stmt.execute(sql);

	} catch (SQLException e)
	{
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} finally
	{
	    stmt.close();
	    con.close();
	}

	// Executing the query
	System.out.println("Executed:");
	System.out.println(sql);

    }
    
}
