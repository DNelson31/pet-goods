package pet.data;

import java.sql.*;
import java.util.*;

import pet.business.*;

public class ProductDB {

    
  //This method returns all products.
    public static List<Product> selectProducts() {
    	List<Product> products = new ArrayList<>();
    	String query = "SELECT * FROM Product";
        try {
        	Connection con = DBUtil.getConnection();
    	    Statement stmt = con.createStatement();
    	    ResultSet rs = stmt.executeQuery(query);
            
            while (rs.next()) {
                Product p = new Product();
                p.setCode(rs.getString("ProductCode"));
                p.setName(rs.getString("ProductName"));
                p.setDescription(rs.getString("ProductDescription"));
                p.setPrice(rs.getDouble("ProductPrice"));
                products.add(p);
            }
            stmt.close();
    	    con.close();
        } catch (Exception e) {
        	e.printStackTrace();
        } 
        return products;
    }
    
  //This method returns all products sorted by price.
    public static List<Product> selectProductsLow() {
    	List<Product> products = new ArrayList<>();
    	String query = "SELECT * FROM Product ORDER by ProductPrice ASC";
        try {
        	Connection con = DBUtil.getConnection();
    	    Statement stmt = con.createStatement();
    	    ResultSet rs = stmt.executeQuery(query);
            
            while (rs.next()) {
                Product p = new Product();
                p.setCode(rs.getString("ProductCode"));
                p.setName(rs.getString("ProductName"));
                p.setDescription(rs.getString("ProductDescription"));
                p.setPrice(rs.getDouble("ProductPrice"));
                products.add(p);
            }
            stmt.close();
    	    con.close();
        } catch (Exception e) {
        	e.printStackTrace();
        } 
        return products;
    }
    
  //This method returns all products sorted by price.
    public static List<Product> selectProductsHigh() {
    	List<Product> products = new ArrayList<>();
    	String query = "SELECT * FROM Product ORDER by Productprice DESC";
        try {
        	Connection con = DBUtil.getConnection();
    	    Statement stmt = con.createStatement();
    	    ResultSet rs = stmt.executeQuery(query);
            
            while (rs.next()) {
                Product p = new Product();
                p.setCode(rs.getString("ProductCode"));
                p.setName(rs.getString("ProductName"));
                p.setDescription(rs.getString("ProductDescription"));
                p.setPrice(rs.getDouble("ProductPrice"));
                products.add(p);
            }
            stmt.close();
    	    con.close();
        } catch (Exception e) {
        	e.printStackTrace();
        } 
        return products;
    }
    
    /*
     * selects a product based on product code
     */
    public static Product selectProduct(String code) {
    	String query = "SELECT * FROM product where productcode = '" + code + "'";
    	Product p = new Product();
		try {
			Connection con = DBUtil.getConnection();
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				//Product p = new Product();
                p.setCode(rs.getString("ProductCode"));
                p.setName(rs.getString("ProductName"));
                p.setDescription(rs.getString("ProductDescription"));
                p.setPrice(rs.getDouble("ProductPrice"));
			}

			stmt.close();
			con.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return p;
	
   }
    
    /*
     * updates product information
     */
    public static void update(Product product) {
    	String query = "UPDATE Product SET "
    			+ "ProductName = '" + product.getName() + "', "
    			+ "ProductDescription = '" + product.getDescription() + "', "
    			+ "ProductPrice = " + product.getPrice() + " "
    			+ "Where ProductCode = '" + product.getCode() + "'";
    	try {
    		DBUtil.execute(query);
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
    
    /*
     * deletes product
     */
    public static void delete(String code) {
    	String query = "DELETE FROM Product WHERE ProductCode = '" + code + "'";
    	try {
    		DBUtil.execute(query);
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
    
    /*
     * inserts product
     */
    
    public static void insert(Product product) {
    	String query = "INSERT into product(productcode, productname, productdescription, productprice) "
    			+ "values ('"
    			+ product.getCode() +"', '"
    			+ product.getName() + "', '"
    			+ product.getDescription() + "', "
    			+ product.getPrice() + ")";
    	try {
    		DBUtil.execute(query);
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
}
