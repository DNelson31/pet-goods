package pet.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;



public class AccountDB {
	/* Checks if login credentials are correct
	 * 
	 * returns true if login credentials are correct, false otherwise
	 */
	public static boolean login(String email, String password, String role) {
		   String query ="Select Email, Password FROM Account Where Email = ? and Password = ? And Role = ?";
		   
		   try {
			   Connection con = DBUtil.getConnection();
			   PreparedStatement ps = null;
		       ResultSet rs = null;
		       
		       ps = con.prepareStatement(query);
	           ps.setString(1, email);
	           ps.setString(2, password);
	           ps.setString(3, role);
	           rs = ps.executeQuery();
	           return rs.next();
		   } catch (Exception e) {
			   e.printStackTrace();
				return false;
		   }
	}
	
		/*
	    * Checks if customer with specified email exists within the database under the specified role
	    * 
	    * return True if a customer with given email exists, false otherwise
	    */
	   public static boolean emailExists(String email, String role) {
		   String query = "SELECT Email FROM Account "
	               + "WHERE Email = ? and role = ?";
		   try {
			   Connection con = DBUtil.getConnection();
			   PreparedStatement ps = null;
		       ResultSet rs = null;
			   ps = con.prepareStatement(query);
	           ps.setString(1, email);
	           ps.setString(2, role);
	           rs = ps.executeQuery();
	           return rs.next();
	           
	          
		   } catch (Exception e) {
			   e.printStackTrace();
				return false;
		   }
	   }
	   
	   /*
	    * inserts user credentials into the database
	    */
	   public static void insert(String email, String password, String role) {
		   String query
	                = "INSERT INTO Account ( Email, Password, Role) "
	                + "VALUES ('" + email + "', '" + password + "', '" + role + "')";

	      	try {
	      		
	      		DBUtil.execute(query);
	            
	      	} catch (Exception e) {
	      		// TODO: handle exception
	      		e.printStackTrace();
	      	}
	   	}
}
