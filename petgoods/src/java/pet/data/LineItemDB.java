package pet.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import pet.business.LineItem;
import pet.business.Product;

public class LineItemDB {
	
	/*
	 * inserts lineitem instance into the database
	 */
	public static long insert(Long invoiceID, LineItem lineItem) {
        

        String query = "INSERT INTO LineItem(invoice_number, ProductID, Quantity) "
                + "VALUES ("
                + invoiceID + ", "
                + lineItem.getProduct().getCode() + ", "
                + lineItem.getQuantity() + ")";
        try {
            
            DBUtil.execute(query);
            return 1;
        } catch (Exception e) {
        	e.printStackTrace();
            return 0;
        } 
    }
	
	
	/*
	 * selects a lineitem based on invoice id
	 */
	public static List<LineItem> selectLineItems(long invoiceID) {


        String query = "SELECT * FROM LineItem "
                + "WHERE Invoice_number = "
                + invoiceID;
        try {
            
        	Connection con = DBUtil.getConnection();
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(query);
            List<LineItem> lineItems = new ArrayList<>();
            while (rs.next()) {
                LineItem lineItem = new LineItem();
                String productID = rs.getString("ProductID");
                Product product = ProductDB.selectProduct(productID);
                lineItem.setProduct(product);
                lineItem.setQuantity(rs.getInt("Quantity"));
                lineItems.add(lineItem);
            }
            stmt.close();
			con.close();
            return lineItems;
            
        } catch (SQLException e) {
            System.err.println(e);
            return null;
        }
        
    }
}
