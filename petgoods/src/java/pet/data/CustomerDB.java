package pet.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

//import .DBUtil;

import pet.business.Customer;


public class CustomerDB {

	
	
	public static Customer selectByEmail(String email) {
    	String query = "SELECT * FROM Customer WHERE email = '"
                         + email + "'";
    	Customer c = new Customer();
		try {
			Connection con = DBUtil.getConnection();
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				
				c.setCustomerId(rs.getLong("CUSTOMERID"));
                c.setFirstName(rs.getString("FIRSTNAME"));
                c.setLastName(rs.getString("LASTNAME"));
                c.setEmail(rs.getString("EMAIL"));
                c.setCompanyName(rs.getString("CompanyName"));
                c.setAddress1(rs.getString("Address1"));
                c.setAddress2(rs.getString("Address2"));
                c.setCity(rs.getString("City"));
                c.setState(rs.getString("ST"));
                c.setPostCode(rs.getString("POSTCODE"));
                c.setCardType(rs.getString("CardType"));
                c.setNumber(rs.getString("CardNumber"));
                c.setExpirationDate(rs.getString("ExpirationDate"));
                
                
			}

			stmt.close();
			con.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
		return c;
	
   }
	

   /*
    *  Inserts customer instance into the database
    */
   public static void insert(Customer customer) {
	   String query
                = "INSERT INTO Customer (FirstName, LastName, Email, CompanyName, "
                + "Address1, Address2, City, Postcode, ST, CardNumber, CardType, ExpirationDate) "
                + "VALUES ('" + customer.getFirstName() + "', '" + customer.getLastName() + "', '" 
                + customer.getEmail() + "', '" + customer.getCompanyName() + "', '"
                + customer.getAddress1() + "', '" + customer.getAddress2() + "', '" 
                + customer.getCity() + "', '" + customer.getPostCode()+ "', '" 
                + customer.getState() + "', '" + customer.getNumber() + "', '" 
                + customer.getCardType() + "', '" + customer.getExpirationDate() + "')";

      	try {
      		Connection con = DBUtil.getConnection();
      		DBUtil.execute(query);
      		String identityQuery = "SELECT CustomerID from customer where email = '" + customer.getEmail() + "'";
            Statement identityStatement = con.createStatement();
            ResultSet identityResultSet = identityStatement.executeQuery(identityQuery);
            identityResultSet.next();
            long userID = identityResultSet.getLong("customerid");
            customer.setCustomerId(userID);
            identityResultSet.close();
            identityStatement.close();
            con.close();
      		
            
      	} catch (Exception e) {
      		// TODO: handle exception
      		e.printStackTrace();
      	}
   	}
   
   /*
    * updates the customer instance
    */
   public static void update(Customer customer) {
	   String query
                = "UPDATE Customer SET "
                		+ "FirstName = '" + customer.getFirstName() + "', "
                		+ "LastName = '" + customer.getLastName() + "', "
                		+ "Email = '" + customer.getEmail() + "', "
                		+ "CompanyName = '" + customer.getCompanyName() + "', "
                		+ "Address1 = '" + customer.getAddress1() + "', "
                		+ "Address2 = '" + customer.getAddress2() + "', "
                		+ "City = '" + customer.getCity() + "', "
                		+ "Postcode = '" + customer.getPostCode() + "', "
                		+ "ST = '" + customer.getState() + "', "
                		+ "CardType = '" + customer.getCardType() + "', "
                		+ "CardNumber = '" + customer.getNumber() + "', "
                		+ "ExpirationDate = '" + customer.getExpirationDate() + "' "
                		+ "WHERE email = '" + customer.getEmail() + "'";  

      	try {
      		DBUtil.execute(query);
      		
      	} catch (Exception e) {
      		// TODO: handle exception
      		e.printStackTrace();
      	}
   	}
   
   /*
    * Checks if customer with specified email exists within the database
    * 
    * return True if a customer with given email exists, false otherwise
    */
   public static boolean emailExists(String email) {
	   String query = "SELECT Email FROM customer "
               + "WHERE Email = ?";
	   try {
		   Connection con = DBUtil.getConnection();
		   PreparedStatement ps = null;
	       ResultSet rs = null;
	       ps = con.prepareStatement(query);
           ps.setString(1, email);
           rs = ps.executeQuery();
           return rs.next();
           
          
	   } catch (Exception e) {
		   e.printStackTrace();
			return false;
	   }
   }
   
  
	 
}
