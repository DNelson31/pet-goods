<%@page contentType="text/html" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
	<head>
		<title>Pet Goods | Admin</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap -->
		<link rel="stylesheet" href="styles/css/bootstrap.min.css"/>
		<link rel="stylesheet" href="styles/css/style.css"  />
      
	</head>
	<body>
		<div class="container">
   			<div class="page-header centre-align">
      			<h1>Admin Panel</h1>
   			</div>
   			<div class="row">
      			<div class="col-sm-9">
         			<c:choose>
            			<c:when test="${Invoices == null}">
               				<h3 class="centre-align">There are no invoices in the database.</h3>
            			</c:when>
            			<c:otherwise>
               				<p class="larger_p">You can view the processed and unprocessed invoices below.</p>
               				<div class="row-2">
	               				<form action="admin">
	               					<input type="submit" value="All Invoices">
	               				</form>
	               				<form action="admin">
	               					<input type="hidden" name="action" value="processed">
	               					<input type="submit" value="Processed Invoices">
	               				</form>
	               				<form action="admin">
	               					<input type="hidden" name="action" value="unprocessed">
	               					<input type="submit" value="Unprocessed Invoices">
	               				</form>
	               				<form>
	               					<input type="hidden" name="action" value="cancelled">
	               					<input type="submit" value="Cancelled Invoices">
	               				</form>
               				</div>
               				<table class="table">
                  				<thead>
                     				<tr>
                        				<th>Invoice Number</th>
                        				<th>Customer</th>
                        				<th>Invoice Date</th>
                        				<th>Status</th>
                        				<th>&nbsp;</th>
                     				</tr>
                  				</thead>
                  				<tbody>
                     				<c:forEach var="invoice" items="${Invoices}">
                        				<tr>
                           					<td>${invoice.number}</td>
                           					<td>${invoice.customer.firstName} ${invoice.customer.lastName}</td>
                           					<td>${invoice.invoiceDate}</td>
                           					<td>
                              					${invoice.processed }
                           					</td>
                           					<td>
                              					<!-- <a href="displayAdminInvoice" class="btn btn-default">View</a> -->
                              					<form action="displayAdminInvoice">
                              						
										    		<input type="hidden" name="number" value="${invoice.number}">
										    		<input type="submit" value="View">
                              					</form>
                           					</td>
                        				</tr>
                     				</c:forEach>
                  				</tbody>
               				</table>
            			</c:otherwise>
         			</c:choose>
      			</div>
      			<div class="col-sm-3">
         			<ul class="nav nav-pills nav-stacked">
            			<li class="active"><a href="admin">View Invoices</a></li>
            			<li><a href="adminProducts">View Products</a></li>
            			<li><a href="adminLogout">Logout</a></li>
         			</ul>
      			</div>
   			</div>   
		</div>
	</body>
</html>