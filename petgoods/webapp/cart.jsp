<%@page contentType="text/html" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>Pet Goods | Products</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="styles/main.css">
    </head>
    <body>
        <div class="header">
            <div class="container">
                <div class="navbar">
                    <div class="logo">
                        
                        <h1>Pet Goods</h1>
                    </div>
                    <nav>
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li><a href="Products">Products</a></li>
                            <li><a href="login">Account</a></li>
                            <li><a href="admin-login.jsp">Admin</a></li>
                        </ul>
                    </nav>
                    <a href="Order">
                        <img src="images/cart.png" width="30px" height="30px">
                    </a>
                </div>
            </div>
        </div>
        <c:choose>
      		<c:when test="${emptyMessage != null}">
         		<h3>${emptyMessage}</h3>
         		<a href="<c:url value='Products' />" class="btn btn-primary">Continue shopping</a>
      		</c:when>
      	<c:otherwise>
        <a href="<c:url value='Products' />" class="btn btn-primary">Continue shopping</a>
        <div class="small-container cart-page">
            <table>
                <tr>
                    <th>Product</th>
                    <th>Quantity</th>
                    <th>Subtotal</th>
                </tr>
                <c:forEach var="item" items="${cart.items}">
                <tr>
                    <td>
                        <div class="cart-info">
                            <img src="images/productimages/${ item.product.code}.jpg">
                            <div>
                                <p>${ item.product.name}</p>
                                <small>Price: ${item.product.priceCurrencyFormat }</small>
                                <br>
                                <form method="post" action="removeItem" class="float-left">
            						<input type="hidden" name="productCode" value="<c:out value='${item.product.code}' />" />
            						<input type="submit" class="btn " value="delete" />
         						</form>
                            </div>
                        </div>
                    </td>
                    <td>
                    	<form method="post" action="updateItem">
                    		<input type="hidden" name="productCode" value="<c:out value='${item.product.code}' />" />
                       	 	<input type="number" name="quantity" min="1" value="<c:out value='${item.quantity}' />" />
                        	<input type="submit" class="btn btn-default" value="Update" />
                    	</form>
                    </td>
                    <td>${item.totalPriceCurrencyFormat }</td>
                </tr>
                </c:forEach>
            </table>
            <div class="total-price">
                <table>
                    <tr>
                        <td>Subtotal</td>
                        <td>${cart.subTotalPriceCurrencyFormat }</td>
                    </tr>
                    <tr>
                        <td>Tax</td>
                        <td>${cart.taxCurrencyFormat}</td>
                    </tr>
                    <tr>
                        <td>Shipping</td>
                        <td>${cart.shippingCurrencyFormat}</td>
                    </tr>
                    <tr>
                        <td>Total</td>
                        <td>${cart.totalPriceCurrencyFormat}</td>
                    </tr>
                    <tr>
                    	<td>
                    		<form method="post" action="checkCustomer">
        						<input type="submit" class="btn btn-primary" value="Checkout" />
        					</form>
        				</td>
                    </tr>
                </table>
            </div>
        </div>
        </c:otherwise>
   </c:choose>
    </body>
</html>
