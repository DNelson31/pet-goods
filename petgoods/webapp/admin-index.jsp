<%@page contentType="text/html" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
	<head>
		<title>Pet Goods | Admin</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap -->
		<link rel="stylesheet" href="styles/css/bootstrap.min.css"/>
		<link rel="stylesheet" href="styles/css/style.css"  />
      

	</head>
	<body>
		<div class="container">
   			<div class="page-header centre-align">
      			<h1>Admin Panel</h1>
   			</div>
   			<div class="row">
      			<div class="col-sm-9">
         			<p class="larger_p">
            			This is your admin panel from which you can manage your web store. 
         			</p>
      			</div>
      			<div class="col-sm-3">
         			<ul class="nav nav-pills nav-stacked">
            			
            			<li><a href="admin">View Invoices</a></li>
            			<li><a href="adminProducts">View Products</a></li>
            			<li><a href="adminLogout">Logout</a></li>
         			</ul>
      			</div>
   			</div>   
		</div>
	</body>
</html>