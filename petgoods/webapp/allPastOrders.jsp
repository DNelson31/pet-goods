<%@page contentType="text/html" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
	<head>
		<title>Pet Goods | Past Orders</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap -->
		<link rel="stylesheet" href="styles/css/bootstrap.min.css"/>
		<link rel="stylesheet" href="styles/css/style.css"  />
      
	</head>
	<body>
		<div class="container">
			<div class="navbar">
                    <div class="logo">
                        
                        <h1>Pet Goods</h1>
                    </div>
                    <nav>
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li><a href="Products">Products</a></li>
                            <li><a href="login">Account</a></li>
                            <li><a href="admin-index.jsp">Admin</a></li>
                        </ul>
                    </nav>
                    <a href="Order">
                        <img src="images/cart.png" width="30px" height="30px">
                    </a>
                </div>
   			<div class="page-header centre-align">
      			<h1>Past Orders</h1>
   			</div>
   			<div class="row">
      			<div class="col-sm-9">
         			<c:choose>
            			<c:when test="${customerInvoices == null}">
               				<h3 class="centre-align">You have no past orders</h3>
            			</c:when>
            			<c:otherwise>
               				<p class="larger_p">You can view all your past orders below.</p>
               				<table class="table">
                  				<thead>
                     				<tr>
                        				<th>Invoice Number</th>
                        				<th>Customer</th>
                        				<th>Invoice Date</th>
                        				<th>Status</th>
                        				<th>&nbsp;</th>
                     				</tr>
                  				</thead>
                  				<tbody>
                     				<c:forEach var="invoice" items="${customerInvoices}">
                        				<tr>
                           					<td>${invoice.number}</td>
                           					<td>${invoice.customer.firstName} ${invoice.customer.lastName}</td>
                           					<td>${invoice.invoiceDate}</td>
                           					<td>
                              					${invoice.processed }
                           					</td>
                           					<td>
                              					
                              					<form action="displayPastOrder">
                              						
										    		<input type="hidden" name="number" value="${invoice.number}">
										    		<input type="submit" value="View">
                              					</form>
                           					</td>
                        				</tr>
                     				</c:forEach>
                  				</tbody>
               				</table>
            			</c:otherwise>
         			</c:choose>
      			</div>
      			<div class="col-sm-3">
         			<ul class="nav nav-pills nav-stacked">
            			<li class="active"><a href="account-index.jsp">Go back to main menu</a></li>
            			
         			</ul>
      			</div>
   			</div>   
		</div>
	</body>
</html>