<%@page contentType="text/html" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
	<head>
		<title>Pet Goods | Past Order</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap -->
		<link rel="stylesheet" href="styles/css/bootstrap.min.css"/>
		<link rel="stylesheet" href="styles/css/style.css"  />
		

	</head>
	<body>
		
		<div class="container">
			<div class="navbar">
                    <div class="logo">
                        
                        <h1>Pet Goods</h1>
                    </div>
                    <nav>
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li><a href="Products">Products</a></li>
                            <li><a href="login">Account</a></li>
                            <li><a href="admin-login.jsp">Admin</a></li>
                        </ul>
                    </nav>
                    <a href="Order">
                        <img src="images/cart.png" width="30px" height="30px">
                    </a>
                </div>
   			<div class="page-header centre-align">
      			<h1>Past Order</h1>
   			</div>
   			<div class="row">
      			<div class="col-sm-9 margin-bottom">
         			<div class="col-sm-10 col-sm-offset-1">
            			<div class="panel panel-default larger_p additional-padding">
               				<p><strong>Date:</strong></p>
               				<p>${invoice.invoiceDate}</p>

               				<p><strong>Ship to:</strong></p>
               				<p class="remove-margin"><c:out value='${invoice.customer.firstName} ${invoice.customer.lastName}' /></p>
               				<c:if test="${invoice.customer.companyName != null}">
                  				<p class="remove-margin"><c:out value='${invoice.customer.companyName}' /></p>
               				</c:if>
               				<p class="remove-margin"><c:out value='${invoice.customer.address1}' /></p>
               				<c:if test="${invoice.customer.address2 != null}">
                  				<p class="remove-margin"><c:out value='${invoice.customer.address2}' /></p>
               				</c:if>
               				<p class="remove-margin"><c:out value='${invoice.customer.city}' /></p>
               				<p class="remove-margin"><c:out value='${invoice.customer.state}' /></p>
              				<p class="remove-margin"><c:out value='${invoice.customer.postCode}' /></p>

               				<table class="table">
                  				<colgroup>
                     				<col class="col-md-2">
                     				<col class="col-md-8">
                     				<col class="col-md-2">
                  				</colgroup>
                  				<thead>
                     				<tr>
                        				<th>Quantity</th>
                        				<th>Name</th>
                        				<th>Price</th>
                     				</tr>
                  				</thead>
                  				<tbody>
                     				<c:forEach var="item" items="${invoice.lineItems}">
                        				<tr>
                           					<td>${item.quantity}</td>
                           					<td>${item.product.name}</td>
                           					<td>${item.totalPriceCurrencyFormat}</td>
                        				</tr>
                     				</c:forEach>
                     				<tr>
                     					<td><strong>SubTotal:</strong></td>
                     					<td>&nbsp;</td>
                     					<td>${invoice.invoiceSubTotalCurrencyFormat}</td>
                  					</tr>
                  					<tr>
                     					<td><strong>Tax:</strong></td>
                     					<td>&nbsp;</td>
                     					<td>${invoice.invoiceTaxCurrencyFormat}</td>
                  					</tr>
                  					<tr>
                     					<td><strong>Shipping:</strong></td>
                     					<td>&nbsp;</td>
                     					<td>${invoice.invoiceShippingCurrencyFormat}</td>
                  					</tr>
                     				<tr>
                        				<td><strong>Total:</strong></td>
                        				<td>&nbsp;</td>
                        				<td>${invoice.invoiceTotalCurrencyFormat}</td>
                     				</tr>   
                  				</tbody>
               				</table>
            			</div>
            			<c:if test="${invoice.processed == 'Not Shipped'}">
			              
			               
			               <button class="btn btn-danger" onclick="document.getElementById('id01').style.display='block'">Cancel Order</button>

							<div id="id01" class="modal">
							  <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">×</span>
							  <form class="modal-content" action="PastOrders" method="post">
							    <div class="container">
							      <h1>Cancel Order</h1>
							      <p>Are you sure you want to cancel your order?</p>
							    
							      <div class="clearfix">
							        <button type="button" onclick="document.getElementById('id01').style.display='none'" class="cancelbtn">No</button>
							        <button type="submit" class="deletebtn">Yes</button>
							        <input type="hidden" name="action" value="cancel">
			                  		<input type="hidden" name="invoiceNumber" value="<c:out value='${invoice.number}' />" />
							      </div>
							    </div>
							  </form>
							</div>
			               
			            </c:if>
			         </div>
			      </div>
			      <div class="col-sm-3">
			        <ul class="nav nav-pills nav-stacked">
			        	<li class="active"><a href="PastOrders">Go back to past orders</a></li>
			    	</ul>
			 	</div>
			</div>   
		</div>
	</body>
</html>