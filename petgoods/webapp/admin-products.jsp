<%@page contentType="text/html" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	
	<head>
		<title>Pet Goods | Admin</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap -->
		<link rel="stylesheet" href="styles/css/bootstrap.min.css"/>
		<link rel="stylesheet" href="styles/css/style.css"  />
      
	</head>
	<body>
		<div class="container">
   			<div class="page-header centre-align">
      			<h1>Admin Panel</h1>
   			</div>
   			<div class="row">
      			<div class="col-sm-9">
               				<p class="larger_p">View and edit products below.</p>
               				<form action="adminAddProduct.jsp">
               					<input type="submit" value="Add Products">
               				</form>
               				<table class="table">
                  				<thead>
                     				<tr>
                        				<th></th>
                        				<th></th>
										<th>Product Name</th>
										<th>Description</th>
										<th>Price</th>
                     				</tr>
                  				</thead>
                  				<tbody>
                     				<c:forEach var="oneProd" items="${ allProductList }">
									<tr>
										<td>
											
											<button class="btn btn-danger" onclick="document.getElementById('${oneProd.code }').style.display='block'">Delete</button>

											<div id="${oneProd.code }" class="modal">
											  <span onclick="document.getElementById('${oneProd.code }').style.display='none'" class="close" title="Close Modal">×</span>
											  <form class="modal-content" action="adminProducts" method="post">
											    <div class="container">
											      <h1>Delete Product</h1>
											      <p>Are you sure you want to delete this product?</p>
											    
											      <div class="clearfix">
											        <button type="button" onclick="document.getElementById('${oneProd.code }').style.display='none'" class="cancelbtn">No</button>
											        <button type="submit" class="deletebtn">Yes</button>
											        <input type="hidden" name="action" value="deleteproduct">
							                  		<input type="hidden" name="productCode" value="<c:out value='${oneProd.code}' />" />
											      </div>
											    </div>
											  </form>
											</div>
										</td>
										<td>
											<form method="get" action="adminProducts">
												<input type="hidden" name="action" value="editproduct">
												<input type="hidden" name="productCode" value="${oneProd.code }">
												<input type="submit" value="Edit">
											</form>
										</td>
										<td>${ oneProd.name }</td>
										<td>${ oneProd.description }</td>
										<td>${ oneProd.price }</td>
									</tr>
									</c:forEach>
                  				</tbody>
               				</table>
      			</div>
      			<div class="col-sm-3">
         			<ul class="nav nav-pills nav-stacked">
            			<li><a href="admin">View Invoices</a></li>
            			<li class="active"><a href="adminProducts">View Products</a></li>
            			<li><a href="adminLogout">Logout</a></li>
         			</ul>
      			</div>
   			</div>   
		</div>
	</body>
</html>