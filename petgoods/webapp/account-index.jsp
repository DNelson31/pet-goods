<%@page contentType="text/html" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
	<head>
		<title>Pet Goods | Account</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap -->
		<link rel="stylesheet" href="styles/css/bootstrap.min.css"/>
		<link rel="stylesheet" href="styles/css/style.css"  />
		<link rel="stylesheet" href="styles/main.css">
      
	</head>
	<body>
		<div class="container">
			<div class="navbar">
                    <div class="logo">
                        <!--<img src="images/index.png" width="125px">-->
                        <h1>Pet Goods</h1>
                    </div>
                    <nav>
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li><a href="Products">Products</a></li>
                            <li><a href="about.html">About</a></li>
                            <li><a href="admin-login.jsp">Admin</a></li>
                        </ul>
                    </nav>
                    <a href="Order">
                        <img src="images/cart.png" width="30px" height="30px">
                    </a>
                </div>
   			<div class="page-header centre-align">
      			<h1>Welcome ${customer.firstName }</h1>
   			</div>
   			<div class="row">
      			<div class="col-sm-9">
         			<p class="larger_p">
            			Manage your account. 
         			</p>
      			</div>
      			<div class="col-sm-3">
         			<ul class="nav nav-pills nav-stacked">
            			
            			<li><a href="PastOrders">Past Orders</a></li>
            			<li><a href="editAccount">Edit account</a></li>
            			<li><a href="logout">Log out</a></li>
         			</ul>
      			</div>
   			</div>   
		</div>
	</body>
</html>