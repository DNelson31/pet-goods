<%@page contentType="text/html" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
    <head>
        <title>Pet Goods | Products</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="styles/main.css">
    </head>
    <body>
        <div class="header">
            <div class="container">
                <div class="navbar">
                    <div class="logo">
                        
                        <h1>Pet Goods</h1>
                    </div>
                    <nav>
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li><a href="Products">Products</a></li>
                            <li><a href="login">Account</a></li>
                            <li><a href="admin-index.jsp">Admin</a></li>
                        </ul>
                    </nav>
                    <a href="Order">
                        <img src="images/cart.png" width="30px" height="30px">
                    </a>
                </div>
            </div>
        </div>
        
        <div class="small-container">
            <div class="row row-2">
                <h2>All Products</h2>
                
                <form action="Products" method="get">
                	<input type="hidden" name="action" value="lowtohigh">
                	<input type="submit" value="Low to High">
                </form>
                
                <form action="Products" method="get">
                	<input type="hidden" name="action" value="hightolow">
                	<input type="submit" value="High to Low">
                </form>
                
                
            </div>
            <div class="row">   
            <c:forEach var="oneProd" items="${ allProductList }">   
				<div class="col-4">
                	
                	<img src="images/productimages/${oneProd.code}.jpg">
                    <h4>${oneProd.name}</h4>
                    <p>$${oneProd.price}</p>
                    <form name="showproduct" method="get" action="SingleProduct">
						<input type="hidden" name="productCode" value="${ oneProd.code }">
						<input type="submit" value="More" />
					</form>
               	</div>           
            </c:forEach>
        	</div>
        </div>
    </body>
</html>
