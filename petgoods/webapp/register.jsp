<%@page contentType="text/html" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>Pet Goods | Login</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="styles/main.css">
    </head>
    <body>
        <div class="header">
            <div class="container">
                <div class="navbar">
                    <div class="logo">
                        <!--<img src="images/index.png" width="125px">-->
                        <h1>Pet Goods</h1>
                    </div>
                    <nav>
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li><a href="Products">Products</a></li>
                            <li><a href="login">Account</a></li>
                            <li><a href="admin-login.jsp">Admin</a></li>
                        </ul>
                    </nav>
                    <a href="Order">
                        <img src="images/cart.png" width="30px" height="30px">
                    </a>
                </div>
            </div>
        </div>
        
        <div class="account-page">
            <div class="container">
                <div class="row">
                    <div class="col-2">
                        <img src="images/login.jpg" alt="">
                    </div>
                    <div class="col-2">
                        <div class="form-container">
                            <div class="form-btn">
                                <span>Register</span>
                            </div>
                            <form action="registerAccount" method="post">
                            	<c:if test="${message != null}">
         							<h3>${message}</h3>
      							</c:if>
                                <input type="text" placeholder="First Name" name="firstname" required>
                                <input type="text" placeholder="Last Name" name="lastname" required>
                                <input type="email" placeholder="Email" name="email" required>
                                <input type="password" placeholder="Password" name="password"required>
                                <input type="password" placeholder="Confirm Password" name="password2" required>
                                <button type="submit" class="btn">Register</button>
                                <a href="login.jsp">Already have an account</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
