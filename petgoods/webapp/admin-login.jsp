<%@page contentType="text/html" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
    <head>
        <title>Pet Goods | Login</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="styles/main.css">
    </head>
    <body>
        <div class="header">
            <div class="container">
                <div class="navbar">
                    <div class="logo">
                        <!--<img src="images/index.png" width="125px">-->
                        <h1>Pet Goods</h1>
                    </div>
                    <nav>
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li><a href="Products">Products</a></li>
                            <li><a href="login">Account</a></li>
                            <li><a href="admin-login.jsp">Admin</a></li>
                        </ul>
                    </nav>
                    <a href="Order">
                        <img src="images/cart.png" width="30px" height="30px">
                    </a>
                </div>
            </div>
        </div>
        
        <div class="account-page">
            <div class="container">
                <div class="row">
                    <div class="col-2">
                        <img src="images/login.jpg" alt="">
                    </div>
                    <div class="col-2">
                        <div class="form-container">
                            <div class="form-btn">
                                <span>Login</span>
                            </div>
                            <form action = "login" method = "post">
                                <c:if test="${message != null}">
         							<h3>${message}</h3>
      							</c:if>
      							<input type="hidden" name="action" value="adminlogin">
                                <input type="email" placeholder="Email" name = "email" required>
                                <input type="password" placeholder="Password" name = "password" required>
                                <button type="submit" class="btn">Login</button>
                                <br>
                                <a href="register.jsp">Register</a>
                            </form>                         
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
