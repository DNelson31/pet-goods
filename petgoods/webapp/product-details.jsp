<%@page contentType="text/html" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
    <head>
        <title>Pet Goods | Products</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="styles/main.css">
    </head>
    <body>
        <div class="header">
            <div class="container">
                <div class="navbar">
                    <div class="logo">
                        
                        <h1>Pet Goods</h1>
                    </div>
                    <nav>
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li><a href="Products">Products</a></li>
                            <li><a href="login">Account</a></li>
                            <li><a href="admin-login.jsp">Admin</a></li>
                        </ul>
                    </nav>
                    <a href="Order">
                        <img src="images/cart.png" width="30px" height="30px">
                    </a>
                </div>
            </div>
        </div>
  
        
        
        <div class="small-container single-product">
            <div class="row">
                <div class="col-2">
                    <img src="images/productimages/${allProductList.code}.jpg" width="100%">
                </div>
                <div class="col-2">
                    <p>Home</p>
                    <h1>${allProductList.name}</h1>
                    <h4>$${allProductList.price}</h4>
                    <form method="post" action="addItem" class="float-left">
                    	<input type="number" name = "quantity" value="1" min="1">
                    
            			<input type="hidden" name="productCode" value="<c:out value='${allProductList.code}' />" />
            			<input type="submit" class="btn btn-primary" value="Add to Cart" />
         			</form>
                </div>
            </div>
        </div>
        
       
    </body>
</html>
