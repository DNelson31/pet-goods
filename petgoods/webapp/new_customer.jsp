<%@page contentType="text/html" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
    <head>
        <title>Pet Goods | Products</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="styles/main.css">
    </head>
    <body>
        <div class="header">
            <div class="container">
                <div class="navbar">
                    <div class="logo">
                        
                        <h1>Pet Goods</h1>
                    </div>
                    <nav>
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li><a href="Products">Products</a></li>
                            <li><a href="login">Account</a></li>
                            <li><a href="admin-login.jsp">Admin</a></li>
                        </ul>
                    </nav>
                    <a href="Order">
                        <img src="images/cart.png" width="30px" height="30px">
                    </a>
                </div>
            </div>
        </div>
        
        
        
        <h1>Enter your name and contact information</h1>

		<div class = "form" style="margin-top: 0px;">
		<form action="processCustomer" method=post >
    		<p id="required">Required <span class="required">*</span></p>
    
    		<label>First Name</label>
    		<input type="text" name="firstName"  maxlength=20 
               		value="${customer.firstName}" required>
    		<p class="asterisk">*</p><br>
    
    		<label>Last Name</label>
    		<input type="text" name="lastName" value="${customer.lastName}" required>
    		<p class="asterisk">*</p><br>
    
    		<label>Email Address</label>
    		<input type="email" name="email" value="${customer.email}" required>
    		<p class="asterisk">*</p><br>
    
    		<label>Company</label>
    		<input type="text" name="companyName" value="${customer.companyName}">
    		<p class="required">&nbsp;</p><br>
    
    		<label>Address1</label>
    		<input type="text" name="address1" value="${customer.address1}" required> 
    		<p class="asterisk">*</p><br>
    
    		<label>Address2</label>
    		<input type="text" name="address2" value="${customer.address2}">
    		<p class="required">&nbsp;</p><br>
    
    		<label>City</label>
    		<input type="text" name="city" value="${customer.city}" required>
    		<p class="asterisk">*</p><br>
    
    		<label>State</label>
    		<br>
    		<select name="state" required>
    			<option value="${customer.state}"></option>
    			<option value="Alabama">Alabama</option>
    			<option value="Alaska">Alaska</option>
    			<option value="Arizona">Arizona</option>
    			<option value="Arkansas">Arkansas</option>
    			<option value="California">California</option>
    			<option value="Colorado">Colorado</option>
    			<option value="Connecticut">Connecticut</option>
    			<option value="Delaware">Delaware</option>
    			<option value="Florida">Florida</option>
    			<option value="Georgia">Georgia</option>
    			<option value="Hawaii">Hawaii</option>
    			<option value="Idaho">Idaho</option>
    			<option value="Illinois">Illinois</option>
    			<option value="Indiana">Indiana</option>
    			<option value="Iowa">Iowa</option>
    			<option value="Kansas">Kansas</option>
    			<option value="Kentucky">Kentucky</option>
    			<option value="Louisiana">Louisiana</option>
    			<option value="Maine">Maine</option>
    			<option value="Maryland">Maryland</option>
    			<option value="Massachusetts">Massachusetts</option>
    			<option value="Michigan">Michigan</option>
    			<option value="Minnesota">Minnesota</option>
    			<option value="Mississippi">Mississippi</option>
    			<option value="Missouri">Missouri</option>
    			<option value="Montana">Montana</option>
    			<option value="Nebraska">Nebraska</option>
    			<option value="Nevada">Nevada</option>
    			<option value="New Hampshire">New Hampshire</option>
    			<option value="New Jersey">New Jersey</option>
    			<option value="New Mexico">New Mexico</option>
    			<option value="New York">New York</option>
    			<option value="North Carolina">North Carolina</option>
    			<option value="North Dakota">North Dakota</option>
    			<option value="Ohio">Ohio</option>
    			<option value="Oklahoma">Oklahoma</option>
    			<option value="Oregon">Oregon</option>
    			<option value="Pennsylvania">Pennsylvania</option>
    			<option value="Rhode Island">Rhode Island</option>
    			<option value="South Carolina">South Carolina</option>
    			<option value="South Dakota">South Dakota</option>
    			<option value="Tennessee">Tennessee</option>
    			<option value="Texas">Texas</option>
    			<option value="Utah">Utah</option>
    			<option value="Vermont">Vermont</option>
    			<option value="Virginia">Virginia</option>
    			<option value="Washington">Washington</option>
    			<option value="West Virginia">West Virginia</option>
    			<option value="Wisconsin">Wisconsin</option>
    			<option value="Wyoming">Wyoming</option>
  			</select>
    		<p class="asterisk">*</p><br>
    
    		<label>Zip Code</label>
    		<input type="number" name="postCode" value="${customer.postCode}" required>
    		<p class="asterisk">*</p><br>
    
    		<label>&nbsp;</label>
    		<input type="submit" value="Continue">
		</form>
		</div>