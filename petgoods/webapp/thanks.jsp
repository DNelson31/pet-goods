<%@page contentType="text/html" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
	<head>
		<title>Pet Goods | Products</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap -->
		<link rel="stylesheet" href="styles/css/bootstrap.min.css"/>
		<link rel="stylesheet" href="styles/css/style.css"  />
	</head>
	<body>
		
		<div class="container">
   			<div class="page-header centre-align">
      			<h1>Thank you for your purchase, <c:out value='${name}' />!</h1>
   			</div>
   			<p class="larger_p">
      		Your order has been submitted. We will begin processing your 
      		order right away. If you have any questions about your order, 
      		please feel free to contact us at 
      		<a href="mailto:petgoods@gmail.com">petgoods@gmail.com</a>
      		<br>
      		<a href="index.html">Return to home page</a>
   			</p>
		</div>
	</body>
</html>