<%@page contentType="text/html" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
		<title>Pet Goods | Products</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="styles/main.css">
	</head>
	<body>
	<h1>Update product information</h1>
	<form action="adminProducts" method="post" class = "form" style="margin-top: 0px;">
		<input type="hidden" name="action" value="updateproduct">
		<input type="hidden" name="productCode" value="${allProductList.code }" required>
		
		<label>Product Name:</label>
		<input type="text" name="productName" value="${allProductList.name }" required>
		
		<label>Product Description:</label>
		<input type="text" name="productDescription" value="${allProductList.description }" required>
		
		<label>Product Price:</label>
		<input type="number" step="0.01" min="0" name="productPrice" value="${allProductList.price }" required>		
		<input type="submit" value="Edit">
	</form>
	
	</body>
</html>