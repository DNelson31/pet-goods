<%@page contentType="text/html" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
		<title>Pet Goods | Products</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="styles/main.css">
	</head>
	<body>
	<h1>Add Product</h1>
	<form action="adminProducts" method="post" class = "form" style="margin-top: 0px;">
		<input type="hidden" name="action" value="addproduct">
		<label>Product Code:</label>
		<input type="text" name="productCode" required>
		
		<label>Product Name:</label>
		<input type="text" name="productName" required>
		
		<label>Product Description:</label>
		<input type="text" name="productDescription" required>
		
		<label>Product Price:</label>
		<input type="number" step="0.01" min="0" name="productPrice" required>
		<input type="submit" value="Add">
	</form>
	</body>
</html>